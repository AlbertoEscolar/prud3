package com.alber.prud3;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Objects;

@Entity
public class Destino {
    private int idDestino;
    private String localizacion;
    private String horario;
    private Integer poblacion;

    @Id
    @Column(name = "id_destino")
    public int getIdDestino() {
        return idDestino;
    }

    public void setIdDestino(int idDestino) {
        this.idDestino = idDestino;
    }

    @Basic
    @Column(name = "localizacion")
    public String getLocalizacion() {
        return localizacion;
    }

    public void setLocalizacion(String localizacion) {
        this.localizacion = localizacion;
    }

    @Basic
    @Column(name = "horario")
    public String getHorario() {
        return horario;
    }

    public void setHorario(String horario) {
        this.horario = horario;
    }

    @Basic
    @Column(name = "poblacion")
    public Integer getPoblacion() {
        return poblacion;
    }

    public void setPoblacion(Integer poblacion) {
        this.poblacion = poblacion;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Destino destino = (Destino) o;
        return idDestino == destino.idDestino &&
                Objects.equals(localizacion, destino.localizacion) &&
                Objects.equals(horario, destino.horario) &&
                Objects.equals(poblacion, destino.poblacion);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idDestino, localizacion, horario, poblacion);
    }
}
