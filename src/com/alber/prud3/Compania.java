package com.alber.prud3;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Objects;

@Entity
public class Compania {
    private int idCompania;
    private String nombreCompania;
    private String ciudadCompania;
    private Integer empleados;

    @Id
    @Column(name = "id_compania")
    public int getIdCompania() {
        return idCompania;
    }

    public void setIdCompania(int idCompania) {
        this.idCompania = idCompania;
    }

    @Basic
    @Column(name = "nombre_compania")
    public String getNombreCompania() {
        return nombreCompania;
    }

    public void setNombreCompania(String nombreCompania) {
        this.nombreCompania = nombreCompania;
    }

    @Basic
    @Column(name = "ciudad_compania")
    public String getCiudadCompania() {
        return ciudadCompania;
    }

    public void setCiudadCompania(String ciudadCompania) {
        this.ciudadCompania = ciudadCompania;
    }

    @Basic
    @Column(name = "empleados")
    public Integer getEmpleados() {
        return empleados;
    }

    public void setEmpleados(Integer empleados) {
        this.empleados = empleados;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Compania compania = (Compania) o;
        return idCompania == compania.idCompania &&
                Objects.equals(nombreCompania, compania.nombreCompania) &&
                Objects.equals(ciudadCompania, compania.ciudadCompania) &&
                Objects.equals(empleados, compania.empleados);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idCompania, nombreCompania, ciudadCompania, empleados);
    }
}
