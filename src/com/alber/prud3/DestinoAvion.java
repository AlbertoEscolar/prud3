package com.alber.prud3;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "destino_avion", schema = "aeropuertohibernate", catalog = "")
@IdClass(DestinoAvionPK.class)
public class DestinoAvion {
    private int idDestinoU;
    private int idAvionU;

    @Id
    @Column(name = "id_destinoU")
    public int getIdDestinoU() {
        return idDestinoU;
    }

    public void setIdDestinoU(int idDestinoU) {
        this.idDestinoU = idDestinoU;
    }

    @Id
    @Column(name = "id_avionU")
    public int getIdAvionU() {
        return idAvionU;
    }

    public void setIdAvionU(int idAvionU) {
        this.idAvionU = idAvionU;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DestinoAvion that = (DestinoAvion) o;
        return idDestinoU == that.idDestinoU &&
                idAvionU == that.idAvionU;
    }

    @Override
    public int hashCode() {
        return Objects.hash(idDestinoU, idAvionU);
    }
}
