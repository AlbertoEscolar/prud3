package com.alber.prud3;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Objects;

@Entity
public class Avion {
    private int idAvion;
    private String modelo;
    private String fabricante;
    private Integer plazas;

    @Id
    @Column(name = "id_avion")
    public int getIdAvion() {
        return idAvion;
    }

    public void setIdAvion(int idAvion) {
        this.idAvion = idAvion;
    }

    @Basic
    @Column(name = "modelo")
    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    @Basic
    @Column(name = "fabricante")
    public String getFabricante() {
        return fabricante;
    }

    public void setFabricante(String fabricante) {
        this.fabricante = fabricante;
    }

    @Basic
    @Column(name = "plazas")
    public Integer getPlazas() {
        return plazas;
    }

    public void setPlazas(Integer plazas) {
        this.plazas = plazas;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Avion avion = (Avion) o;
        return idAvion == avion.idAvion &&
                Objects.equals(modelo, avion.modelo) &&
                Objects.equals(fabricante, avion.fabricante) &&
                Objects.equals(plazas, avion.plazas);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idAvion, modelo, fabricante, plazas);
    }
}
