package com.alber.prud3;

import javax.persistence.*;
import java.sql.Date;
import java.util.Objects;

@Entity
public class Pasajero {
    private int idPasajero;
    private String nombrePasajero;
    private Date fechaNacimiento;
    private Integer edad;
    private Compania id_pasajero;

    @Id
    @Column(name = "id_pasajero")
    public int getIdPasajero() {
        return idPasajero;
    }

    public void setIdPasajero(int idPasajero) {
        this.idPasajero = idPasajero;
    }

    @Basic
    @Column(name = "nombre_pasajero")
    public String getNombrePasajero() {
        return nombrePasajero;
    }

    public void setNombrePasajero(String nombrePasajero) {
        this.nombrePasajero = nombrePasajero;
    }

    @Basic
    @Column(name = "fecha_nacimiento")
    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    @Basic
    @Column(name = "edad")
    public Integer getEdad() {
        return edad;
    }

    public void setEdad(Integer edad) {
        this.edad = edad;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pasajero pasajero = (Pasajero) o;
        return idPasajero == pasajero.idPasajero &&
                Objects.equals(nombrePasajero, pasajero.nombrePasajero) &&
                Objects.equals(fechaNacimiento, pasajero.fechaNacimiento) &&
                Objects.equals(edad, pasajero.edad);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idPasajero, nombrePasajero, fechaNacimiento, edad);
    }

    @ManyToOne
    @JoinColumn(name = "id_pasajero", referencedColumnName = "id_pasajeroC", nullable = false)
    public Compania getId_pasajero() {
        return id_pasajero;
    }

    public void setId_pasajero(Compania id_pasajero) {
        this.id_pasajero = id_pasajero;
    }
}
