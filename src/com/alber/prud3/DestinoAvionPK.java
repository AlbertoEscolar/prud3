package com.alber.prud3;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Objects;

public class DestinoAvionPK implements Serializable {
    private int idDestinoU;
    private int idAvionU;

    @Column(name = "id_destinoU")
    @Id
    public int getIdDestinoU() {
        return idDestinoU;
    }

    public void setIdDestinoU(int idDestinoU) {
        this.idDestinoU = idDestinoU;
    }

    @Column(name = "id_avionU")
    @Id
    public int getIdAvionU() {
        return idAvionU;
    }

    public void setIdAvionU(int idAvionU) {
        this.idAvionU = idAvionU;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DestinoAvionPK that = (DestinoAvionPK) o;
        return idDestinoU == that.idDestinoU &&
                idAvionU == that.idAvionU;
    }

    @Override
    public int hashCode() {
        return Objects.hash(idDestinoU, idAvionU);
    }
}
